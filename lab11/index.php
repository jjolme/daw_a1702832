<?php
function JLOG($msg) {
	echo $msg."<br>";
}
/* Parses the model from a string and outputs the model as a 'json' */
function parseModel($modelStr) {
	$lines = explode("\n", $modelStr);
	$nInputs  = intval(explode(" ", $lines[0])[0]);
	$nOutputs = intval(explode(" ", $lines[0])[1]);

	$params = explode(" ", $lines[1]);
	for ($i=0; $i < count($params); $i++) {
		$params[$i] = intval($params[$i]);
	}

	return array (
		"nIns"  => $nInputs,
		"nOuts" => $nOutputs,
		"params" => $params
	);
	
}
/* Makes a Linear Regression Prediction using the model parameters and the input values */
function predict($model, $in) {
	$h = 0;

	$h += $model['params'][0]; // Bias

	for ($i=0; $i < count($in); $i++) {
		$h += $in[$i] * $model['params'][$i+1]; // Params Mult
	}

	return $h;
}


$fStr = file_get_contents("./model.txt");
$model = parseModel($fStr);
?>


<?php 

	$labNum = 11;
	$labTitle = "Manejo de Formas y modelo de capas";
	include("header.php"); 

	if (isset($_GET['submit'])) {

		$x = array(count($model['params']));

		for ($i=0; $i < count($x); $i++)
			$x[$i] = $_GET["x".$i];

		JLOG("<h2>El resultado de la predicción anterior es: ".predict($model, $x)."</h2>");
	}

	include("body.php");	

?>