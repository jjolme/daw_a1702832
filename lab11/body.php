
	<form action="#" method="GET">
		<?php
			for ($i=0; $i < count($model['params'])-1; $i++) {

				echo "<label for='".$i."'>Input ".$i.":</label>".
					 "<input type='number' name='x".$i."'><br>";
			}
		?>
		<input type="submit" name="submit">
	</form>



	<!-- Start Section Questions -->
		<div class="separator" id="questions-separator">
			<h2>Questions</h2>
		</div>

		<article id="preg-1">
			<h2>¿Por qué es una buena práctica separar el controlador de la vista?</h2>
			<div>
				<p>
					En general es por el principio de separación de intereses. <br>
					Es más sencillo trabajar en el diseño de una aplicación/sistema si se define la funcionalidad de cada módulo ya que hace más natural el desarrollo y programación.
					<br>
					Además ayuda a reducir el acoplamiento del código. Esto es importante para evitar que errores en una parte, como la lógica, afectan a otra supuestamente independiente, como la vista.
					<br>
					Por lo anterior, es más fácil el desarrollo y mantenimiento de un proyecto.
				</p>
			</div>
		</article>

		<article id="preg-2">
			<h2>Aparte de los arreglos $_POST y $_GET, ¿qué otros arreglos están predefinidos en php y cuál es su función?</h2>
			<div>
				<p>
					Tambien existen el las variables: $_SESSION, $_COOKIE, $_ENV, $_SERVER, $GLOBALS, $_FILES.[3]
					<br>
					De las más relevantes son $_SESSION que guarda variables definidas por el script para guardar información de un usuario. $_COOKIE contiene variables cookie que se guardan en el navegador del usuario y $_ENV contiene configuraciones del ambiente de donde se ejecuta PHP.
				</p>
			</div>
		</article>

		<article id="preg-3">
			<h2>Explora las funciones de php, y describe 2 que no hayas visto en otro lenguaje y que llamen tu atención.</h2>
			<div>
				<p>
					Muy pocos lenguajes vienen integrados con librerías para utilizar bases de datos SQL, por lo que se me hacen muy interesantes el set de funciones que tiene PHP. <br>
					Entre ellas estan:
				</p>
				<ul>
					<li>mysqli_connect(host,user,pass,bd) para conectarse a una base de datos</li>
					<li>mysqli_quiery(conn,queryStr,mode) para ejecutar queries en la base de datos.</li>
				</ul>
				<p>Otras funciones interesantes son respecto a seguridad. [2]</p>
				<ul>
					<li>md5(str) Encripta usando el algoritmo "Message Digest Algorithm 5"</li>
					<li>sha1(str) Encripta usando el "Secure Hash Algorithm"</li>
					<li>hash(str) Da un hash tradicional de la cadena.</li>
				</ul>
			</div>
		</article>

		<article id="preg-4">
			<h2>¿Qué es XSS y cómo se puede prevenir?</h2>
			<div>
				<p>
					Cross Site Scripting es un tipo de ataque web en donde un agente malicioso, 'inyecta' código de cliente que posteriormente se ejecuta al renderizar la página. Por ejemplo, el atacante introduce en un campo código html válido, se guarda en el servidor y cuando este lo manda para su renderización, el navegador lo interpeta. <br>
					Aunque hay más variantes. Por ejemplo meter el url de un archivo contenga código.
				</p>
				<br>
				<p>La mejor mánera de prevenirlo es parsando cualquier entrada del cliente, para substituir cualquier representación de código, con una literal del caracter. Dos funciones que hacen esto en PHP son: <em>htmlentities(str)</em> y <em>htmlspecialchars(str)</em></p>
			</div>
		</article>
		<!-- End Section Questions -->
		<br>

		<footer>
			<h2>Bibliografía:</h2>
			<ul>
				<li>[1] <a href="https://medium.com/@jpmorris/how-to-prevent-xss-attacks-by-escaping-output-in-php-942204bf184">https://medium.com/@jpmorris/how-to-prevent-xss-attacks-by-escaping-output-in-php-942204bf184</a></li>
				<li>[2] <a href="https://joshtronic.com/2013/06/06/encrypting-passwords-in-php/">https://joshtronic.com/2013/06/06/encrypting-passwords-in-php/</a></li>
				<li>[3] <a href="http://php.net/manual/es/language.variables.superglobals.php">http://php.net/manual/es/language.variables.superglobals.php</a></li>
				
			</ul>
		</footer>

	</div>
</body>
</html>