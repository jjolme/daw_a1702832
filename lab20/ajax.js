
function sendRequest() {

	var userInput = $('#userinput').val();
	$.ajax({
		url: "ssajax",
		data: { pattern: userInput},
		success: function(data) {
			var htmlStr = "";
			for (var i = 0; i < data.length; i++) {
				htmlStr += "<option value='"+data[i]+"'>"+data[i]+"</option>";
			}
			$("#ajaxResponse").html("<select onchange='showSelect(this)'>" + htmlStr + "</select>");

			loadImage(data[0]);
		},
		dataType: 'json'
	});
}

function loadImage(fruit) {
	console.log("Load img " + fruit);
	$.ajax({
		url: "ssimages",
		data: { name: fruit},
		success: function(data) {
			console.log("Server Response: " + data);
			var htmlStr = "";
			for (var i = 0; i < data.length; i++) {
				htmlStr += "<img src='images/"+data[i]+"'>";
			}
			$("#imageContainer").html(htmlStr);
		},
		dataType: 'json'
	});
}



function showSelect(e) {
	
	loadImage(e.value);
}




