<?php 

function connectDB() {

	$host = "127.0.0.1";
	$user = "root";
	$pass = "r00t23021998DE.jjol";
	$dbname = "fruits_db";

	$conn = mysqli_connect($host, $user, $pass, $dbname);

	if(!$conn)
		die ("Connection failed! ". mysqli_connect_error());

	return $conn;

}

function closeDB($conn) {
	mysqli_close($conn);
}

function getFruitImages($fruitName) {

	$imgs = array();

	$conn = connectDB();

	$sql = "SELECT * FROM Images WHERE name LIKE '%$fruitName%'";
	
	$res = mysqli_query($conn, $sql);

	if (mysqli_num_rows($res) > 0) {
		while ($row = mysqli_fetch_assoc($res)) {
			array_push($imgs, $row['imgName']);
		}
	}


	closeDB($conn);

	return $imgs;
}


function getAllFruits() {
	$conn = connectDB();

	$sql = "SELECT name, units, quantity, price, country FROM Fruit";

	$res = mysqli_query($conn, $sql);

	closeDB($conn);

	return $res;
}

function getFruitsByName($name) {
	$conn = connectDB();

	$sql = "SELECT name, units, quantity, price, country FROM Fruit WHERE name LIKE '%".$name."%'";

	$res = mysqli_query($conn, $sql);

	closeDB($conn);

	return $res;
}

function getFruitsCheaperThan($price) {
	$conn = connectDB();

	$sql = "SELECT name, units, quantity, price, country FROM Fruit WHERE price < '" . $price . "'";

	$res = mysqli_query($conn, $sql);

	closeDB($conn);

	return $res;
}

 ?>