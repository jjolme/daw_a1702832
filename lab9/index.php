<!DOCTYPE html>
<html>
<head>
	<title>Lab 9</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="page">
		<header>
			<h1>Lab #9: Orientado a Eventos</h1>
			<h4>Por Juan José Olivera - A01702832</h4>
		</header>
		<br>

		<!-- Start Section Questions -->
		<div class="separator" id="questions-separator">
			<h2>Programs:</h2>
		</div>
		<!-- End Section Questions -->
		<br>

	<?php

	function JLOG($msg) {
		echo $msg."<br>";
	}

	function promedio($arr) {
		$total = 0;
		$size = count($arr);
		for ($i = 0; $i < $size; $i++) {
			$total += $arr[$i];
		}

		return $total / $size;
	}

	function sort_arr($arr, $type) {
		$copy = $arr;
		$size = count($copy);

		for ($i = $size; $i > 0; $i--) {
			for ($j = 0; $j < $i-1; $j++) {
				if ($type == S_DESC) {
					if ($copy[$j] < $copy[$j + 1]) {
						swap($copy, $j, $j+1);
					}
				} else {
					if ($copy[$j] > $copy[$j + 1]) {
						swap($copy, $j, $j+1);
					}
				}
			}
		}

		return $copy;
	}

	function mediana($arr) {
		$size = count($arr);

		$sorted = sort_arr($arr, S_ASC);

		return $sorted[$size/2];
	}

	function swap(&$arr, $i, $j) {
		$temp = $arr[$i];
		$arr[$i] = $arr[$j];
		$arr[$j] = $temp;
	}

	function printArrayInfo($arr) {

		$size = count($arr);

		echo "<ul>";

		echo "<li><strong>Arreglo:</strong></li>";
		for ($i = 0; $i < $size; $i++) {
			echo "<li>";
			echo 	$arr[$i];
			echo "</li>";
		}

		echo "<li><strong>Media:</strong>"  .promedio($arr)."</li>";
		echo "<li><strong>Mediana:</strong>".mediana($arr) ."</li>";

		$sortedStr  = "<li>[";
		$sortedArrAsc = sort_arr($arr, S_ASC);
		for ($i = 0; $i < $size; $i++) {
			$sortedStr .= $sortedArrAsc[$i];
			if ($i != $size - 1)
				$sortedStr .= ",";
		}
		$sortedStr .= "]</li>";
		echo $sortedStr;

		$sortedStr  = "<li>[";
		$sortedArrDesc = sort_arr($arr, S_DESC);
		for ($i = 0; $i < $size; $i++) {
			$sortedStr .= $sortedArrDesc[$i];
			if ($i != $size - 1)
				$sortedStr .= ",";
		}
		$sortedStr .= "]</li>";
		echo $sortedStr;


		echo "</ul>";
	}

	function tablaMultiplicar($n) {
		echo "<table>";
		echo 	"<thead>";
		echo 		"<th>X</th>";
		echo 		"<th>X^2</th>";
		echo 		"<th>X^3</th>";
		echo 	"</thead>";
		echo "<tbody>";

		for ($i = 0; $i <= $n; $i++) {
			echo "<tr>";
			echo "<td>".$i."</td>";
			echo "<td>".$i*$i."</td>";
			echo "<td>".$i*$i*$i."</td>";
			echo "</tr>";
		}

		echo "</tbody>";
		echo "</table>";
	}


	/* Reads model from file and makes a prediction with input x */
	function tellmeModel($path, $x) {
		$fStr = file_get_contents("./".$path);
		$model = parseModel($fStr);

		$h = predict($model, $x);

		return $h;
	}

	/* Parses the model from a string and outputs the model as a 'json' */
	function parseModel($modelStr) {
		$lines = explode("\n", $modelStr);
		$nInputs  = intval(explode(" ", $lines[0])[0]);
		$nOutputs = intval(explode(" ", $lines[0])[1]);

		$params = explode(" ", $lines[1]);
		for ($i=0; $i < count($params); $i++) {
			$params[$i] = intval($params[$i]);
		}

		return array (
			"nIns"  => $nInputs,
			"nOuts" => $nOutputs,
			"params" => $params
		);
		
	}

	/* Makes a Linear Regression Prediction using the model parameters and the input values */
	function predict($model, $in) {
		$h = 0;

		$h += $model['params'][0]; // Bias

		for ($i=0; $i < count($in); $i++) {
			$h += $in[$i] * $model['params'][$i+1]; // Params Mult
		}

		return $h;
	}

	JLOG("<h3>Programa de Promedio de arreglo: [1,2,3,4,5,6,7,8,9,10]</h3>");
	JLOG("El promedio es: ".promedio(array(1,2,3,4,5,6,7,8,9,10)));

	JLOG("<h3>Programa de Mediana de arreglo: [3,1,4,2,1.5]</h3>");
	JLOG("Mediana es: " . mediana(array(3,1,4,2,1.5)));

	JLOG("<h3>Programa de Información de Arreglo</h3>");
	JLOG(printArrayInfo([41,24,51,365,56,3,46,2]));

	JLOG("<h3>Programa de Tablas de Multiplicar hasta el 13</h3>");
	tablaMultiplicar(13);

	JLOG("<h3>"."Programa Libre, Modelos de Machine Learning (Regresion Lineal):</h3>");

	$fStr = file_get_contents("./model.txt");
	$model = parseModel($fStr);
	$h = predict($model, [32,12,2,1]);

	JLOG("N Inputs: ".$model['nIns']." - N Outputs: ".$model['nOuts']);
	echo "Params: "; print_r($model['params']);
	JLOG("<br>");
	JLOG("<strong>Model Prediction for h(x=(32,12,2,1)) = ".$h."</strong>")

?>
<!-- Start Section Questions -->
		<div class="separator" id="questions-separator">
			<h2>Questions</h2>
		</div>

		<article id="preg-1">
			<h2>¿Qué hace la función phpinfo()? Describe y discute 3 datos que llamen tu atención.</h2>
			<div>
				<p>
					La función <em>phpinfo()</em> es utilizadas para imprimir en una tabla toda la información del servidor y procesador de PHP usado [1]. <br>
					Me sorprendio ver que dentro de la información detallada, se puede observar información de módulos de SQL así como el famoso conector sqli de PHP. También hay información de soporte de sockets en php y del comando curl que no sabía que estaba disponible en PHP.
				</p>
			</div>
		</article>

		<article id="preg-2">
			<h2>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?</h2>
			<div>
				<p>
					Entre los cambios sugeridos[2] a cambiar. para llevar el proyecto a una fase de producción y asegurar la calidad e integridad del sistema, se encuentran:
				</p>
				<ul>
					<li>Cambiar el tiempo del recolector de basura para que termine las sesiones de clientes en un tiempo razonable.</li>
					<li>Desactivar funciones y clases de PHP para que no puedan ser equivocamente ejecutados por equivocación o malicia.</li>
					<li>Desactivar el despliegue de errores de PHP para que no haya una retroalimentación para algún atacante.</li>
					<li>Ajustar limite de tamaño de archivos reducidos o tiempos de conexión entre otras cosas.</li>
				</ul>
			</div>
		</article>

		<article id="preg-3">
			<h2>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del servidor? Explica.</h2>
			<div>
				<p>
					El navegador hace una petición de recursos al servidor web que busca el archivo del recurso. Una vez que encuentra el archivo, si es .php lo manda al procesador PHP. Este se encarga de producir una respuesta al copia todo el HTML y ejecutar el código entre las etiquetas de PHP (&lt;?php &gt;). El código ejecutado puede ser que agregue contenido a la respueta en producción. Una vez que finaliza, se lo devuelve al servidor web que lo envia de vuelta al navegador para ser renderizado.[3]
				</p>
			</div>
		</article>
		<!-- End Section Questions -->
		<br>

		<footer>
			<h2>Bibliografía:</h2>
			<ul>
				<li>[1] <a href="http://php.net/manual/es/function.phpinfo.php">http://php.net/manual/es/function.phpinfo.php</a></li>
				<li>[2] <a href="https://www.dummies.com/programming/php/common-php-configuration-changes/">https://www.dummies.com/programming/php/common-php-configuration-changes/</a></li>
				<li>[3] <a href="https://www.phpkolkata.com/blog/features-web-server-php-make-everyone-love/">https://www.phpkolkata.com/blog/features-web-server-php-make-everyone-love/</a></li>
				
			</ul>
		</footer>

	</div>
</body>
</html>

