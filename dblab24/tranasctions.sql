-- Definicion de las Tablas
CREATE TABLE ClientesBanca (
	NoCuenta varchar(5) PRIMARY KEY,
	Nombre   varchar(30),
	Saldo    numeric(10,2)
)

CREATE TABLE TiposMovimiento (
	ClaveM      varchar(2) PRIMARY KEY,
	Descripcion varchar(30)
)

GO

CREATE TABLE Realizan (
	NoCuenta varchar(5),
	ClaveM   varchar(2),
	Fecha    datetime,
	Monto    numeric(10,2),
	PRIMARY KEY (NoCuenta,ClaveM,Fecha),
	FOREIGN KEY (NoCuenta) REFERENCES ClientesBanca(NoCuenta),
	FOREIGN KEY (ClaveM)   REFERENCES TiposMovimiento(ClaveM)
)


-- Transacciones

BEGIN TRANSACTION Prueba1
INSERT INTO ClientesBanca VALUES ('001','Manuel Rios Maldonado', 9000);
INSERT INTO ClientesBanca VALUES ('002','Pablo Perez Ortiz', 5000);
INSERT INTO ClientesBanca VALUES ('003','Luis Flores Alvarado', 8000);
COMMIT TRANSACTION Prueba1

SELECT * FROM ClientesBanca

BEGIN TRANSACTION PRUEBA2 
INSERT INTO ClientesBanca VALUES('004','Ricardo Rios Maldonado',19000); 
INSERT INTO ClientesBanca VALUES('005','Pablo Ortiz Arana',15000); 
INSERT INTO ClientesBanca VALUES('006','Luis Manuel Alvarado',18000); 
ROLLBACK TRANSACTION PRUEBA2
COMMIT TRANSACTION Prueba2

BEGIN TRANSACTION PRUEBA3 
INSERT INTO TiposMovimiento VALUES('A','Retiro Cajero Automatico'); 
INSERT INTO TiposMovimiento VALUES('B','Deposito Ventanilla'); 
COMMIT TRANSACTION PRUEBA3 

BEGIN TRANSACTION PRUEBA4 
INSERT INTO Realizan VALUES('001','A',GETDATE(),500); 
UPDATE ClientesBanca SET SALDO = SALDO -500 
WHERE NoCuenta='001' 
COMMIT TRANSACTION PRUEBA4 

BEGIN TRANSACTION PRUEBA5 
INSERT INTO ClientesBanca VALUES('005','Rosa Ruiz Maldonado',9000);
INSERT INTO ClientesBanca VALUES('006','Luis Camino Ortiz',5000); 
INSERT INTO ClientesBanca VALUES('001','Oscar Perez Alvarado',8000); 

BEGIN TRANSACTION PRUEBA5 
INSERT INTO ClientesBanca VALUES('005','Rosa Ruiz Maldonado',9000);
INSERT INTO ClientesBanca VALUES('006','Luis Camino Ortiz',5000); 
INSERT INTO ClientesBanca VALUES('001','Oscar Perez Alvarado',8000); 


IF @@ERROR = 0 
COMMIT TRANSACTION PRUEBA5 
ELSE 
BEGIN 
PRINT 'A transaction needs to be rolled back' 
ROLLBACK TRANSACTION PRUEBA5 
END 

SELECT * FROM ClientesBanca

SELECT * FROM TiposMovimiento

-- Procedures
GO

IF EXISTS (SELECT name FROM sysobjects
		   WHERE name = 'REGISTRAR_RETIRO_CAJERO' AND type = 'P')
	DROP PROCEDURE REGISTRAR_RETIRO_CAJERO;

GO

CREATE PROCEDURE REGISTRAR_RETIRO_CAJERO
	@NoCuenta varchar(5),
	@Monto    numeric(10,2)
AS
	BEGIN TRANSACTION RegistroRetiro
	INSERT INTO Realizan VALUES (@NoCuenta, 'A', GETDATE(),@Monto);
	UPDATE ClientesBanca SET Saldo = Saldo - @Monto WHERE NoCuenta=@NoCuenta;
	IF @@ERROR = 0
	COMMIT TRANSACTION RegistroRetiro
	ELSE
	BEGIN
	PRINT 'A transaction error ocurred! Performing a Rollback'
	ROLLBACK TRANSACTION RegistroRetiro
	END

GO 

IF EXISTS (SELECT name FROM sysobjects
		   WHERE name = 'REGISTRAR_DEPOSITO_VENTANILLA' AND type = 'P')
	DROP PROCEDURE REGISTRAR_DEPOSITO_VENTANILLA;

GO

CREATE PROCEDURE REGISTRAR_DEPOSITO_VENTANILLA
	@NoCuenta varchar(5),
	@Monto    numeric(10,2)
AS
	BEGIN TRANSACTION RegistroIngreso
	INSERT INTO Realizan VALUES (@NoCuenta, 'B', GETDATE(),@Monto);
	UPDATE ClientesBanca SET Saldo = Saldo + @Monto WHERE NoCuenta=@NoCuenta;

	IF @@ERROR = 0
	COMMIT TRANSACTION RegistroIngreso
	ELSE
	BEGIN
	PRINT 'A transaction error ocurred! Performing a Rollback'
	ROLLBACK TRANSACTION RegistroIngreso
	END
	
GO 

SELECT * FROM ClientesBanca

EXECUTE REGISTRAR_RETIRO_CAJERO '001', 10000;