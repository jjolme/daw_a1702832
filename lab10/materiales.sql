BULK INSERT a1702832.a1702832.[Materiales]
	FROM 'e:\wwwroot\a1702832\materiales.csv'
	WITH
	(
	CODEPAGE='ACP',
	FIELDTERMINATOR=',',
	ROWTERMINATOR='\n'
	)

BULK INSERT a1702832.a1702832.[Proveedores]
	FROM 'e:\wwwroot\a1702832\proveedores.csv'
	WITH
	(
	CODEPAGE='ACP',
	FIELDTERMINATOR=',',
	ROWTERMINATOR='\n'
	)

BULK INSERT a1702832.a1702832.[Proyectos]
	FROM 'e:\wwwroot\a1702832\proyectos.csv'
	WITH
	(
	CODEPAGE='ACP',
	FIELDTERMINATOR=',',
	ROWTERMINATOR='\n'
	)

SET DATEFORMAT dmy
BULK INSERT a1702832.a1702832.[Entregan]
	FROM 'e:\wwwroot\a1702832\entregan.csv'
	WITH
	(
	CODEPAGE='ACP',
	FIELDTERMINATOR=',',
	ROWTERMINATOR='\n'
	)