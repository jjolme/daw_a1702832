<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>TECx - Nuevo</title>
	<link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
	<a href="/lab16/">
	<h1>TECx</h1>
	<h2>Lessons worth sharing</h2>
	</a>
	<div id="web-container">
		<table>
			<caption><?= $taller->nombre ?></caption>
			<tbody>
				<tr>
					<td>Duracion:</td>
					<td><?= $taller->duracion ?></td>
				</tr>
				<tr>
					<td>Costo:</td>
					<td><?= $taller->costo ?>$</td>
				</tr>
				<tr>
					<td>Instructor:</td>
					<td><?= $taller->instructor ?></td>
				</tr>
				<tr>
					<td>Objetivo:</td>
					<td><?= $taller->objetivo ?></td>
				</tr>
				<tr>
					<td>Descripcion:</td>
					<td><?= $taller->descripcion ?></td>
				</tr>
				<tr>
					<td>Audiencia:</td>
					<td><?= $taller->audiencia ?></td>
				</tr>
			</tbody>
		</table>
	</div>
	
</body>
</html>