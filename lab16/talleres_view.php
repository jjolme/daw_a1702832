<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>TECx</title>
	<link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
	<a href="/lab16/">
	<h1>TECx</h1>
	<h2>Lessons worth sharing</h2>
	</a>
	
	<div id="talleres-container">
	<?php $i=1; ?>
	<?php foreach ($talleres as $taller): ?>

		<div class="taller-card" id="taller-<?= $i++; ?>">
			<a href="/lab16/taller_info.php?taller=<?= $taller->id ?>"></a>
			<div class="image-card"></div>
			<div class="header-card">
				<h4><?= $taller->nombre ?></h4>
				<p>Instructor: <?= $taller->instructor ?><br>
				   Duracion: <?= $taller->duracion ?><br>
				   Objetivo: <?= $taller->objetivo ?><br>
				</p>

			</div>
			
		</div>

	<?php endforeach; ?>
	</div>


	<a href="/lab16/taller_create.php">
	<div id="new-btn">
		+
	</div>
	</a>

	<!-- <div id="web-container">
		<h3>Discover Your Passion</h3>
	</div> -->


	      <!--JavaScript at end of body for optimized loading-->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
      <script>M.AutoInit()</script>
      <script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
      <script>
          let cards = document.querySelectorAll('.taller-card');

          for (let i=0; i < cards.length; i++) {
          	cards[i].style.transition = 'opacity 1s ' + (i*0.5) + 's';
          	cards[i].getBoundingClientRect();
          	cards[i].classList.add('active');
          }
      </script>
</body>
</html>