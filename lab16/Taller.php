<?php 

require_once('database.php');

class Taller {
	static function getAll() {
		$conn = connectDB();


		$sql = "SELECT * FROM Talleres";
		$res = mysqli_query($conn, $sql);

		$talleres = array();

		if (mysqli_num_rows($res) > 0) {
			while ($row = mysqli_fetch_assoc($res)) {
				$tall = new Taller();
				$tall->id        = $row['Id_Taller'];
				$tall->nombre    = $row['Nombre'];
				$tall->audiencia = $row['Audiencia'];
				$tall->duracion  = $row['Duracion'];
				$tall->costo     = $row['Costo']; 
				$tall->instructor = $row['Instructor'];
				$tall->objetivo   = $row['Objetivo'];
				$tall->descripcion = $row['Descripcion'];
				array_push($talleres, $tall);
			}
		}

		closeDB($conn);

		return $talleres;
	}

	static function findById($tallerId) {
		$conn = connectDB();


		$sql = "SELECT * FROM Talleres WHERE Id_Taller=$tallerId";
		$res = mysqli_query($conn, $sql);

		if (mysqli_num_rows($res) > 0) {
			$row = mysqli_fetch_assoc($res);
			$tall = new Taller();
				$tall->id        = $row['Id_Taller'];
				$tall->nombre    = $row['Nombre'];
				$tall->audiencia = $row['Audiencia'];
				$tall->duracion  = $row['Duracion'];
				$tall->costo     = $row['Costo']; 
				$tall->instructor = $row['Instructor'];
				$tall->objetivo   = $row['Objetivo'];
				$tall->descripcion = $row['Descripcion'];
			return $tall;

		} else {
			return false;
		}

		closeDB($conn);
	}

	static function createNew($taller) {
		$conn = connectDB();
		$sql = "INSERT INTO Talleres (Nombre, Audiencia, Duracion, Costo, Instructor, Objetivo, Descripcion) VALUES ('$taller->nombre', '$taller->audiencia', '$taller->duracion', '$taller->costo', '$taller->instructor', '$taller->objetivo', '$taller->descripcion')";
		$res = mysqli_query($conn,$sql);
		return $res;
	}

	
	static function makeFake($n) {
		$t = new Taller();
		$t->nombre = "Taller Fake ".$n;
		$t->duracion = "2:30hrs";
		$t->costo = "0";
		$t->audiencia = "Cualquiera";
		$t->instructor = "Juan Jose";
		$t->objetivo = "Fortaleces el Pensamiento Creativo";
		$t->descripcion = "Taller de Imaginación";
		return $t;
	}

	public $id;
	public $nombre;
	public $duracion;
	public $costo;
	public $audiencia;
	public $fotoUrl;

	public $instructor;

	public $fechaCreacion;
	public $objetivo;
	public $descripcion;

	public $agenda;

	public $sesiones;


}

 ?>