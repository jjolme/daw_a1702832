<?php 

require_once ('Taller.php');

if (isset($_POST['submit'])) {
	
	$taller = new Taller();
	$taller->nombre = $_POST['nombre'];
	$taller->audiencia = $_POST['audiencia'];
	$taller->costo = $_POST['costo'];
	$taller->duracion = $_POST['duracion'];
	$taller->instructor = $_POST['instructor'];
	$taller->objetivo  = $_POST['objetivo'];
	$taller->descripcion  = $_POST['descripcion'];

	$res = Taller::createNew($taller);

	if ($res !== false)
		header('Location: /lab16');
	else
		include('taller_create_view.php');


}
else {

	include('taller_create_view.php');

}

?>