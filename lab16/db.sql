CREATE DATABASE IF NOT EXISTS Talleres_DB;

USE Talleres_DB;

DROP TABLE IF EXISTS Talleres;

CREATE TABLE Talleres (
	Id_Taller int not null AUTO_INCREMENT PRIMARY KEY,
	Nombre    varchar(50) not null,
	Audiencia varchar(30),
	Duracion  varchar(30) not null,
	Costo     decimal(6,2) not null,
	Instructor varchar(50) not null,
	Objetivo varchar(140) not null,
	Descripcion varchar(512) not null
);

INSERT INTO Talleres (Nombre, Audiencia, Duracion, Costo, Instructor, Objetivo, Descripcion)
VALUES
('Taller de Progra C', 'Ingenieria', '2:00hrs', '50', 'Juan Jose Olivera', 'Reforzar Temas de C', 'Taller de Programacion para practicar C'),
('Taller de Processing', 'Todos', '2:00hrs', '150', 'Juan Jose Olivera', 'Saber programar en Processing', 'Curso de programacion en processing para hacer aplicaciones interactivas'),
('Hell\'s Kitchen', 'Todos', '2:00hrs', '150', 'Juan Jose Olivera', 'Saber programar en Processing', 'Curso de programacion en processing para hacer aplicaciones interactivas'),
('Vitrales y Mirrales', 'Todos', '2:00hrs', '150', 'Juan Jose Olivera', 'Saber programar en Processing', 'Curso de programacion en processing para hacer aplicaciones interactivas'),
('Educación y Narración', 'Todos', '2:00hrs', '150', 'Juan Jose Olivera', 'Saber programar en Processing', 'Curso de programacion en processing para hacer aplicaciones interactivas'),
('Finanzas Personales Well Done', 'Todos', '2:00hrs', '150', 'Juan Jose Olivera', 'Saber programar en Processing', 'Curso de programacion en processing para hacer aplicaciones interactivas');