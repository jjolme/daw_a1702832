<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>TECx - Nuevo</title>
	<link rel="stylesheet" type="text/css" href="main.css">

</head>
<body>
	<a href="/lab16/">
	<h1>TECx</h1>
	<h2>Lessons worth sharing</h2>
	</a>
	<div id="web-container">
		<h1>Nuevo Taller:</h1>
		<form action="taller_create.php" method="POST">
			<div class="form-group">
			<label for="nombre">Nombre:</label>
			<input type="text" name="nombre" id="nombre"><br>
			</div>
			<div class="form-group">
			<label for="duracion">Duración:</label>
			<input type="text" name="duracion" id="duracion"><br>
			</div>
			<div class="form-group">
			<label for="costo">Costo:</label>
			<input type="text" name="costo" id="costo"><br>
			</div>
			<div class="form-group">
			<label for="audiencia">Audiencia:</label>
			<input type="text" name="audiencia" id="audiencia"><br>
			</div>
			<div class="form-group">
			<label for="instructor">Instructor:</label>
			<input type="text" name="instructor" id="instructor"><br>
			</div>
			<div class="form-group">
			<label for="objetivo">Objetivo:</label>
			<input type="text" name="objetivo" id="objetivo"><br>
			</div>
			<div class="form-group">
			<label for="descripcion">Descripcion:</label>
			<input type="text" name="descripcion" id="descripcion"><br>
			</div>
			<input type="submit" name="submit" value="Crear">

		</form>
	</div>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
	      <script>M.AutoInit()</script>
	      <script
	  src="https://code.jquery.com/jquery-2.2.4.min.js"
	  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
	  crossorigin="anonymous"></script>
</body>
</html>