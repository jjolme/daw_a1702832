/* Validador de Contraseñas */

(function(){


	function validar(pwd1, pwd2) {
		return (pwd1 == pwd2);
	}

	function nivelSeguridad(pwd) {
		let nivel = 0;

		let numberFlag  = false,
			upperFlag   = false,
			lowerFlag   = false,
			specialFlag = false;

		for (let i = 0; i < pwd.length; i++) {
			let c = pwd.charAt(i);
			if ('0' <= c && c <= '9') numberFlag = true;
			if ('A' <= c && c <= 'Z') upperFlag = true;
			if ('a' <= c && c <= 'z') lowerFlag = true;
			if ((' ' <= c && c <= '/') || (':' <= c && c <= '@')) specialFlag = true;
		}

		if (numberFlag) nivel += 25;
		if (upperFlag) nivel += 25;
		if (lowerFlag) nivel += 25;
		if (specialFlag) nivel += 25;

		return nivel;
	}

	let validEl = document.getElementById('ej1-valid');

	let in1El = document.getElementById('ej1-in1'),
		in2El = document.getElementById('ej1-in2');

	in1El.addEventListener('input', () => {
		// Mostrar Nivel de Seguridad cuando la contraseña original cambie
		let secLevel = nivelSeguridad(in1El.value);
		document.getElementById('ej1-progress').value = secLevel;

		// Verficar Coincidencia
		if (validar(in1El.value, in2El.value)) {
			validEl.innerHTML = "Coinciden";
		} else {
			validEl.innerHTML = "No Coinciden";
		}
	});

	in2El.addEventListener('input', () => {
		// Verficar Coincidencia		
		if (validar(in1El.value, in2El.value)) {
			validEl.innerHTML = "Coinciden";
		} else {
			validEl.innerHTML = "No Coinciden";
		}
	});

	let enterBtn = document.getElementById('ej1-btn');
	enterBtn.addEventListener('click', () => {
		if (validar(in1El.value, in2El.value)) {
			alert("Enter Succesfully!");
		} else {
			alert("Password do not Match!");
		}
	});


})();


/* Productos */
(function(){

	let calculateCosts;

	class Producto {
		
		constructor(name, precio, stock) {
			this.name = name;
			this.precio = precio;
			this.stock = stock;

			this.comprados = 0;

			this.qInput = null;
			this.qStock = null;
			this.container = null;
		}

		render() {
			let htmlTemplate = '<h4 id="{nombre_producto}-title">{nombre_producto}</h4>'+
						'<img id="{nombre_producto}-img" src="assets/{nombre_producto}.jpg" alt="Imagen de {nombre_producto}" class="prod-img">'+
						'<br>'+
						'<label>Precio: </label>'+
						'<label id="{nombre_producto}-prec">${precio}</label>'+
						'<br>'+
						'<label>Total Disponible: </label>'+
						'<label id="{nombre_producto}-disp">{disponible}</label>'+
						'<br>'+
						'<label>Encargados: </label>'+
						'<input type="number" id="{nombre_producto}-enca" value="{comprados}">';

			return this.replaceAll(htmlTemplate, '{nombre_producto}', this.name)
					.replace('{precio}', this.precio)
					.replace('{disponible}', this.stock)
					.replace('{comprados}', this.comprados);
		}

		update() {
			// Render Content
			// this.container.innerHTML = this.render();
			// this.container.style.display = 'none';
			// this.container.style.offsetHeight;
			// this.container.style.display = 'block';

			// Remove Elements
			// if (this.qInput) {
			// 	this.qInput.removeEventListener('input', this.onQuantityChange);
			// }

			// // Add Elements
			// this.qInput = document.getElementById(this.name+"-enca");
			// if (!this.qInput) {
			// 	console.log("No Q Input!");
			// }
			// this.qInput.addEventListener('input', this.onQuantityChange.bind(this));


		}

		attachToCatalog(dispEl) {
			this.container = document.createElement('div');

			// Render Content
			this.container.innerHTML = this.render();

			// Add to Page
			dispEl.appendChild(this.container);


			// Binding
			this.qInput = document.getElementById(this.name+"-enca");
			this.qInput.addEventListener('input', this.onQuantityChange.bind(this));

			this.qStock = document.getElementById(this.name+'-disp')
		}

		onQuantityChange() {
			let stockLeft = this.stock - this.qInput.value;
			if (stockLeft < 0) {
				this.qInput.value = this.comprados;
				return;
			}

			this.comprados = this.qInput.value;
			
			this.qStock.innerHTML = stockLeft;	

			calculateCosts();		
		}

		replaceAll(template, match, newStr) {
			let out, prev;

			out = template;

			do {
				prev = out;
				out = prev.replace(match, newStr)
			} while(out != prev);


			return out;
		}
	}

	let pantalones = new Producto("Pantalon", 150, 20);
	let lamparas   = new Producto("Lampara", 700, 17);
	let xboxs      = new Producto("Xbox", 6000, 5);

	
	let dispEl = document.getElementById('ej2-productos-display');
	let precioEl   = document.getElementById('ej2-ptotal'),
		ivaEL      = document.getElementById('ej2-piva'),
		montoEl    = document.getElementById('ej2-mtotal');

	pantalones.attachToCatalog(dispEl);
	lamparas.attachToCatalog(dispEl);
	xboxs.attachToCatalog(dispEl);

	calculateCosts = function() {
		let precioTotal = pantalones.comprados*pantalones.precio +
						  lamparas.comprados*lamparas.precio + 
						  xboxs.comprados*xboxs.precio;
		precioEl.innerHTML = precioTotal + '$';

		let precioIVA  = precioTotal* 0.20; // IVA 20%
		ivaEL.innerHTML = precioIVA + '$';

		let montoTotal = precioTotal + precioIVA;
		montoEl.innerHTML = montoTotal + '$';
	}

})();


/* Tic Tac Toe Game */

(function(){

	let btns = [];

	let matrix = [[0,0,0],[0,0,0],[0,0,0]];
	function ticTacToe(i) {
		i = i - 1;
		let y = Math.floor(i / 3);
		let x = i % 3;

		matrix[y][x] = 1;

		if (matrix[0][0] && matrix[0][1] && matrix[0][2] ||
			matrix[1][0] && matrix[1][1] && matrix[1][2] ||
			matrix[2][0] && matrix[2][1] && matrix[2][2] ||
			matrix[0][0] && matrix[1][0] && matrix[2][0] ||
			matrix[0][1] && matrix[1][1] && matrix[2][1] ||
			matrix[0][2] && matrix[1][2] && matrix[2][2] ||
			matrix[0][0] && matrix[1][1] && matrix[2][2] ||
			matrix[0][2] && matrix[1][1] && matrix[2][0] ) {
			alert("Tic Tac Toe!")
		}
	}

	for (let i = 1; i <= 9; i++) {
		let btnEl = document.getElementById('tt'+i);
		btns.push(btnEl);
		btnEl.addEventListener('click', () => {
			btnEl.style['background-color']='red';

			ticTacToe(i);
		});
	}




})();















