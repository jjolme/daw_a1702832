INSERT INTO Entregan VALUES (0, 'xxx', 0, '1-jan-02', 02)

SELECT * FROM Entregan

DELETE FROM Entregan WHERE Clave = 0

ALTER TABLE Entregan ADD CONSTRAINT cfentreganclave FOREIGN KEY (clave) REFERENCES Materiales(Clave)

ALTER TABLE Entregan ADD CONSTRAINT cfentreganrfc  FOREIGN KEY (RFC) REFERENCES Proveedores (RFC)

ALTER TABLE Entregan ADD CONSTRAINT cfentregannumero FOREIGN KEY (Numero) REFERENCES Proyectos (Numero)
/*
sp_help Entregan 
sp_helpconstraint Entregan 

sp_help Proveedores



SELECT COUNT(*) FROM Proveedores

SELECT COUNT(*) FROM Entregan

SELECT * FROM Entregan
SELECT * FROM Proveedores

SELECT Proveedores.RFC FROM Entregan, Proveedores
WHERE Entregan.RFC = Proveedores.RFC

SELECT RFC FROM Entregan
GROUP BY RFC
*/