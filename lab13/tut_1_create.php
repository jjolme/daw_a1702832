<?php 
session_start();
 ?>

 <!DOCTYPE html>
 <html>
 <head>
 	<title>S. Create</title>
 </head>
 <body>
 <?php 

$_SESSION['favcolor'] = "green";
$_SESSION['favanimal'] = "dog";

echo "Session Vars Set!<br>";
echo "favcolor = " . $_SESSION['favcolor']."<br>";
echo "favanimal = " . $_SESSION['favanimal']."<br>";

  ?>

  <a href="/lab13/tut_1_create.php">Create</a>
  <a href="/lab13/tut_2_read.php">Read</a>
  <a href="/lab13/tut_3_modify.php">Modify</a>
  <a href="/lab13/tut_4_destroy.php">Destroy</a>

 </body>
 </html>