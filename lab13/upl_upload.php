<?php 

$target_dir  = $_SERVER["DOCUMENT_ROOT"]."/lab13/uploads/";
$target_file = $target_dir . basename($_FILES['fileToUpload']['name']);
$uploadOk = 1;

echo "Path of File: " . $target_file . PHP_EOL.PHP_EOL;

$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

if (isset($_POST['submit'])) {
	$check = getimagesize($_FILES['fileToUpload']['tmp_name']);

	if ($check !== false) {
		echo "File is an image - " . $check['mime'] . ".";
		$uploadOk = 1;
	} else {
		echo "File is not an image.";
		$uploadOk = 0;
	}
}

if (file_exists($target_file)) {
	echo "Error file already exists!";
	$uploadOk = 0;
}

if ($_FILES['fileToUpload']['size'] > 500000) {
	echo "Sorry, file is too big!";
	$uploadOk = 0;
}

if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
	echo "Sorry, we don't support your image format!";
	$uploadOk = 0;
}


if ($uploadOk == 0) {
	echo "<br>Your file could not be loaded!";
} else {
	if (move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $target_file)) {
		echo "The file " . basename($_FILES['fileToUpload']['name']) . " has uploaded sucessfully!";
	} else {
		echo "There was an error while uploading the file!";
	}
}

 ?>




