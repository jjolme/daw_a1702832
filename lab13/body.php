
	
	<article>
		<h2>Tutorial 1 de Sesiones</h2>
		<dir><a href="/lab13/tut_1_create.php">Tut 1 Create</a></dir>
	</article>
	<article>
		<h2>Tutorial 2 de Archivos y Fotos</h2>
		<dir><a href="/lab13/upl_1.php">Tut 2 Upload File</a></dir>
	</article>

	<article>
		<h2>Process Go-Through</h2>
		<div>
			<a href="/lab13/tectrain/register_ctrl.php">Ejercicio Personal</a>
		</div>
	</article>


	<!-- Start Section Questions -->
		<div class="separator" id="questions-separator">
			<h2>Questions</h2>
		</div>

		<article id="preg-1">
			<h2>¿Por qué es importante hacer un session_unset() y luego un session_destroy()?</h2>
			<div>
				<p>
					session_unset() limpia las variables de la sesión mientras que session_destroy() remueve el archivo de la sesión. Aunque ya no es primordial usar session_unset() a partir de PHP5, puede ser usado para limpiar las variables de sesión y prevenir que se queden con un valor.
					<br>
					bsession_destroy() remueve el archivo completamente.
					<br>Pero si se ejecuta primero que session_unset(), session_unset() no va a poder limpiar las variables porque la sesión ya no existe [5].
				</p>
			</div>
		</article>

		<article id="preg-2">
			<h2>¿Cuál es la diferencia entre una variable de sesión y una cookie?</h2>
			<div>
				<p>
					Las variables de Sesión y Cookie nos permiten guardar información de un usuario mientras usa nuestro sitio web para no tener que validar en cada página el usuario.
					<br>
					Las variables de Sesión se almacenan en el servidor y generalmente tienen un timpo de vida hasta que el usuario cierra el navegador de Internet. No son accesibles por el cliente.
					<br>
					Las variables Cookie se almacenan en el navegador del usuario y tienen tiempos más prolongados, días, semanas, meses, etc.
					Sin embargo, estas pueden ser modificadas por el cliente, ya sea modificando el archivo o via JavaScript.
					<br>
					Dato Curioso: Las sesiones de PHP necesitan de Cookies para poder identificar el usuario de la petición HTTP y cargar su sesión.
					Cada archivo de sesión en el servidor tiene una cookie en el cliente con este proposito. [1]
				</p>
			</div>
		</article>

		<article id="preg-3">
			<h2>¿Qué técnicas se utilizan en sitios como facebook para que el usuario no sobreescriba sus fotos en el sistema de archivos cuando sube una foto con el mismo nombre?</h2>
			<div>
				<p>
					Guardar archivos con el nombre original puede ser problemático ya que podría haber colisiones con los ya existentes en el servidor. En dicho caso, por confusión se podría perder contenido del sitio, o peor, atacar el sitio modificando archivos críticos.
					Por esto un par de recomendaciones de [2] y [3] son:
				</p>
				<ul>
					<li>Guardar los archivos subidos en un directorio especial fuera de la raiz del servidor.</li>
					<li>Cambiar el nombre del archivo a uno aleatorio para que los atacantes no puedan ciertos archivos.</li>
					<li>Asignar una carpeta por usuario ó agregarle al nombre del archivo el id del usuario y la fecha.</li>
					<li>Validar extensiones y tamaños de archivos.</li>
				</ul>
			</div>
		</article>

		<article id="preg-4">
			<h2>¿Qué es CSRF y cómo puede prevenirse?</h2>
			<div>
				<p>
					Cross Site Request Forgery es un tipo de ataque web en donde se hace engañar un usuario autenticado en un sitio con su navegador, para que haga peticiones perjudiciales al sitio web y que sean validadas por el sitio. [4]
					<br>
					Esto funciona porque los sitios web sólo verifican que el navegador del usuario este autenticado, no que realmente haya sido una acción válida del sitio ejercido por el usuario o que sea en el mismo sitio para empezar.
					<br>
					<ul>
						<li>Primer nivel de protección: usar peticiones POST en operaciones delicadas.</li>
						<li>Segundo nivel: Generar un 'token' aleatorio que será almacenado en el navegador del usuario, y que cada petición deba ser verificada con dicho 'token'.
					<br>
					Funciona porque los atacantes de CSRF no pueden ellos remotamente conseguir información de las cookies para incluirlas en la petición.</li>
					</ul>
				</p>
				
			</div>
		</article>
		<!-- End Section Questions -->
		<br>

		<footer>
			<h2>Bibliografía:</h2>
			<ul>
				<li>[1] <a href="https://canvas.seattlecentral.edu/courses/937693/pages/10-advanced-php-sessions">https://canvas.seattlecentral.edu/courses/937693/pages/10-advanced-php-sessions</a></li>
				<li>[2] <a href="https://stackoverflow.com/questions/24858858/is-it-safe-to-store-a-file-with-its-original-name-on-server-in-php">https://stackoverflow.com/questions/24858858/is-it-safe-to-store-a-file-with-its-original-name-on-server-in-php</a></li>
				<li>[3] <a href="https://www.opswat.com/blog/file-upload-protection-best-practices">https://www.opswat.com/blog/file-upload-protection-best-practices</a></li>
				<li>[4] <a href="https://medium.com/@GemmaBlack/csrf-the-a-z-2b85d26438b">https://medium.com/@GemmaBlack/csrf-the-a-z-2b85d26438b</a></li>
				<li>[5] <a href="https://stackoverflow.com/questions/4303311/what-is-the-difference-between-session-unset-and-session-destroy-in-php">https://stackoverflow.com/questions/4303311/what-is-the-difference-between-session-unset-and-session-destroy-in-php</a></li>
				
				
			</ul>
		</footer>

	</div>
</body>
</html>