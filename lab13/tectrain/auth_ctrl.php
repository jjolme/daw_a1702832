<?php 

function validateAuthorization($endpoint) {
	session_start();

	if (isset($_SESSION['user'])) {
		return true;
	}

	$targetURL = $endpoint;
	include('views/login_view.php');

	return false;
}


 ?>