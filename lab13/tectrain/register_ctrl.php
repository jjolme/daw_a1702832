<?php

	function savePhoto($fotoName, $dest) {

		$target_dir  = $_SERVER["DOCUMENT_ROOT"] . $dest;
		$target_file = $target_dir; //. basename($_FILES[$fotoName]['name']);
		$uploadOk = 1;

		$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

		$check = getimagesize($_FILES[$fotoName]['tmp_name']);

		if ($check !== false) {
			$uploadOk = 1;
		} else {
			$uploadOk = 0;
		}

		if (file_exists($target_file)) {
			$uploadOk = 0;
		}

		if ($_FILES['fileToUpload']['size'] > 500000) {
			$uploadOk = 0;
		}

		if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
			$uploadOk = 0;
		}


		if ($uploadOk == 0) {
			return 0;
		} else {
			if (move_uploaded_file($_FILES[$fotoName]['tmp_name'], $target_file)) {
				return true;
			} else {
				return false;
			}
		}

	}

	include('auth_ctrl.php');

	if (validateAuthorization("register_ctrl.php")) {

		if (!isset($_POST['submit'])) {
			include('views/register_view.php');
		} else {
			// Save Information and photo
			$file = $_FILES['pFoto']['name'];
			
			if (!savePhoto('pFoto', "/lab13/uploads/".$file)) {
				$error = "File could not be uploaded!";
			}

			$pName      = $_POST['pName'];
			$pAnterior  = $_POST['pAnterior'];
			$pSiguiente = $_POST['pSiguiente'];
			$pFoto      = "/lab13/uploads/".$file;

			include('views/consulta_view.php');
		}

		
	}

 ?>