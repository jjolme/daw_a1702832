let paradas = {
	visitantes: {
		name: "Parada Visitantes",
		coords: {x: 320, y: 70},
		prev: 'Parada Prepa-Tec',
		succ: 'Parada Prepa-Tec',
		schedule: [
			{timeArrival: "10:36", dir: 1},
			{timeArrival: "11:00", dir: 0},
			{timeArrival: "14:36", dir: 1},
			{timeArrival: "14:46", dir: 0},
			{timeArrival: "15:16", dir: 1},
			{timeArrival: "15:26", dir: 0},
			{timeArrival: "15:46", dir: 1},
			{timeArrival: "15:56", dir: 0},
			{timeArrival: "16:16", dir: 1}
		]
	},
	prepa: {
		name: "Parada Prepa-Tec",
		coords: {x: 320, y: 125},
		prev: 'Parada Visitantes',
		succ: 'Parada Food Station',
		schedule: [
			{timeArrival: "14:38", dir: 1},
			{timeArrival: "14:48", dir: 0},
			{timeArrival: "15:18", dir: 1},
			{timeArrival: "15:28", dir: 0},
			{timeArrival: "15:48", dir: 1},
			{timeArrival: "15:58", dir: 0},
			{timeArrival: "16:18", dir: 1}
		]
	},
	foodStation: {
		name: "Parada Food Station",
		coords: {x: 320, y: 180},
		prev: 'Parada Prepa',
		succ: 'Parada Borrego',
		schedule: [
			{timeArrival: "14:40", dir: 1},
			{timeArrival: "14:50", dir: 0},
			{timeArrival: "15:20", dir: 1},
			{timeArrival: "15:30", dir: 0},
			{timeArrival: "15:50", dir: 1},
			{timeArrival: "16:00", dir: 0},
			{timeArrival: "16:20", dir: 1}
		]
	},
	borrego: {
		name: "Parada Borrego",
		coords: {x: 170, y: 180},
		prev: 'Parada Food Station',
		succ: 'Parada Oxxo',
		schedule: [
			{timeArrival: "14:42", dir: 1},
			{timeArrival: "14:52", dir: 0},
			{timeArrival: "15:22", dir: 1},
			{timeArrival: "15:32", dir: 0},
			{timeArrival: "15:52", dir: 1},
			{timeArrival: "16:02", dir: 0},
			{timeArrival: "16:22", dir: 1}
		]
	},
	oxxo: {
		name: "Parada Oxxo",
		coords: {x: 170, y: 250},
		prev: 'Parada Borrego',
		succ: 'Parada Ingenieria',
		schedule: [
			{timeArrival: "14:45", dir: 1},
			{timeArrival: "14:55", dir: 0},
			{timeArrival: "15:25", dir: 1},
			{timeArrival: "15:35", dir: 0},
			{timeArrival: "15:55", dir: 1},
			{timeArrival: "16:05", dir: 0},
			{timeArrival: "16:25", dir: 1}
		]
	},
	ingenieria: {
		name: "Parada Ingenieria",
		coords: {x: 100, y: 250},
		prev: 'Parada Oxxo',
		succ: 'Parada Oxxo',
		schedule: [
			{timeArrival: "14:47", dir: 1},
			{timeArrival: "14:57", dir: 0},
			{timeArrival: "15:27", dir: 1},
			{timeArrival: "15:37", dir: 0},
			{timeArrival: "15:57", dir: 1},
			{timeArrival: "16:07", dir: 0},
			{timeArrival: "16:27", dir: 1}
		]
	}
}

function displayParada(parada) {
	let paradaTitleEl  = document.querySelector('#info-container h2');
	let paradaSchedule = document.querySelector('#schedule-body tbody');

	paradaTitleEl.innerHTML = parada.name;

	let currSet = false;

	let htmlStr = "";
	for (let timePoint of parada.schedule) {
		let now =  new Date();
		let time = now.getHours() + ":" + now.getMinutes();

		if (!currSet && timePoint.timeArrival > time) {
			htmlStr += "<tr class='curr-time'>";
			currSet = true;
		}
		else
			htmlStr += "<tr>";
		htmlStr +=	 "<td>" + timePoint.timeArrival + "hrs" + "</td>";
		htmlStr +=	 "<td><i class='material-icons'>" + (timePoint.dir ? "arrow_back" : "arrow_forward") + "</i></td>";
		htmlStr += "</tr>";
	}

	paradaSchedule.innerHTML = htmlStr;
}




let mapaImg   = document.querySelector('#map-container img');
let mapaFrame = document.querySelector('#map-container #map-frame');

let imgRect = mapaImg.getBoundingClientRect();
mapaFrame.style.width  = imgRect.width + "px";
mapaFrame.style.height = imgRect.height + "px";


for (let name in paradas) {
	let parada = paradas[name];
	
	let pMarkEl = document.createElement('div');
	pMarkEl.classList.add('parada-marker');
	pMarkEl.style.top  = parada.coords.y + "px";
	pMarkEl.style.left = parada.coords.x + "px";
	pMarkEl.dataset['name'] = name;
	pMarkEl.onclick = onParadaClick;

	mapaFrame.appendChild(pMarkEl);
}

function onParadaClick(ev) {
	let el = ev.target;
	for (let marker of mapaFrame.children) {
		marker.classList.remove('p-selected');
	}
	el.classList.add('p-selected');
	displayParada(paradas[el.dataset['name']]);
}

displayParada(paradas['borrego']);