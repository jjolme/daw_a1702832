<!DOCTYPE html>
<html>
<head>
	<title>TecTrain</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<!-- Font Source: Libre Baskerville, https://fonts.google.com/specimen/Libre+Baskerville?selection.family=Libre+Baskerville:400,700-->
	<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="views/style.css">
</head>
<body>

	<header>
		<nav class="navbar navbar-light bg-light">
			<h1 id="brand"><a href="#"><span>Tec</span>Train</a></h1>
		</nav>
	</header>

	<div class="container">

		<span class="error"><?php if (isset($error)) echo $error; ?></span>

		<div class="row">
			<div class="col">
				<form action="validate.php" method="POST">
					<label for="usuario">Usuario: </label>
					<input type="text" name="usuario">
					<label for="passwd">Contraseña: </label>
					<input type="password" name="passwd">
					<input type="hidden" name="targetUrl" value="<?= $targetURL ?>">
					<input type="submit" name="submit" value="Entrar">
				</form>
			</div>
			
		</div>
	</div>
	







	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>