<!DOCTYPE html>
<html>
<head>
	<title>Lab <?= $labNum ?></title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="page">
		<header>
			<h1>Lab #<?= $labNum ?>: <?= $labTitle ?></h1>
			<h4>Por Juan José Olivera - A01702832</h4>
		</header>
		<br>

		<!-- Start Section Questions -->
		<div class="separator" id="questions-separator">
			<h2>Programs:</h2>
		</div>
		<!-- End Section Questions -->
		<br>