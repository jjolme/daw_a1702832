
	<article>
		<h2>Fruits Mini App</h2>
		<div>
			<div id="search-container">
				<label for="userinput">Type Fruit Name: </label><input type="userinput" name="userinput" id="userinput" onkeyup="sendRequest()" autocomplete="off" />
				<div id="ajaxResponse"></div>
			</div>

			<div id="imageContainer"></div>
		</div>
	</article>


	<!-- Start Section Questions -->
		<div class="separator" id="questions-separator">
			<h2>Questions</h2>
		</div>

		<article id="preg-1">
			<h2>Explica y elabora un diagrama sobre cómo funciona AJAX con jQuery.</h2>
			<div>
				<p>
					Como ya sabemos, AJAX nos permite mandar peticiones en segundo plano usando objetos XMLHTTPRequest, pero es un procesos tedioso. JQuery facilita extremadamente esto definiendo 3 métodos en su librería: ajax, post y get que utilzan internamente el objeto XMLHTTPRequest.
				</p>
				<img src="JQueryAjax.jpg">
			</div>
		</article>

		<article id="preg-2">
			<h2>¿Qué alternativas a jQuery existen?</h2>
			<div>
				<p>
					En navegadores modernos podemos usar funciones del nuevo standard de JavaScript como por ejemplo la función fetch(url, opts) o el objeto Request que utilizan un sistema de promesas. [2]s
				</p>
				<br>
				<p>Otra alternativa es utilizar librerías y framework frontends como Vue, React, Angular, Poymer, etc., que ya cuentan con funciones para hacer peticiones asincronas.</p>
			</div>
		</article>

		<!-- End Section Questions -->
		<br>

		<footer>
			<h2>Bibliografía:</h2>
			<ul>
				<li>[1] <a href="http://api.jquery.com/jquery.ajax/">http://api.jquery.com/jquery.ajax/</a></li>
				<li>[2] <a href="https://developer.mozilla.org/es/docs/Web/API/Fetch_API/Utilizando_Fetch">https://developer.mozilla.org/es/docs/Web/API/Fetch_API/Utilizando_Fetch</a></li>
			</ul>
		</footer>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script type="text/javascript" src="ajax.js"></script>
	</div>
</body>
</html>