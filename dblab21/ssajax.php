<?php 

require_once('util.php');

$words = array();

$fruits = getAllFruits();

if (mysqli_num_rows($fruits) > 0) {
	while ($row = mysqli_fetch_assoc($fruits)) {
		array_push($words, $row['name']);
	}
}

$pattern = strtolower($_GET['pattern']);



$response = array();
$size = 0;



for ($i=0; $i < count($words); $i++) {
	$pos = stripos(strtolower($words[$i]), $pattern);
	if (!($pos === false)) {
		$size++;
		$word = $words[$i];
		array_push($response, $word);
	}
}

echo json_encode($response);

 ?>