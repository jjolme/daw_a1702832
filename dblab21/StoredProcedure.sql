EXECUTE creaMaterial 5000,'Martillos Acme',250,15
SELECT * FROM Materiales

-- Procedures Para Tabla Materiales
IF EXISTS (SELECT name FROM sysobjects
		   WHERE name = 'creaMaterial' AND type = 'P')
	DROP PROCEDURE creaMaterial;

GO
CREATE PROCEDURE creaMaterial
	@uclave NUMERIC(5,0),
	@udescripcion VARCHAR(50),
	@ucosto NUMERIC(8,2),
	@uimpuesto NUMERIC(6,2)
AS
	INSERT INTO Materiales VALUES (@uclave, @udescripcion, @ucosto, @uimpuesto)

GO

IF EXISTS (SELECT name FROM sysobjects
		   WHERE name = 'modificarMaterial' AND type = 'P')
	DROP PROCEDURE modificarMaterial;

GO
CREATE PROCEDURE modificarMaterial
	@uclave NUMERIC(5,0),
	@udescripcion VARCHAR(50),
	@ucosto NUMERIC(8,2),
	@uimpuesto NUMERIC(6,2)
AS
	UPDATE Materiales 
	SET clave=@uclave, Descripcion=@udescripcion, Costo=@ucosto, PorcentajeImpuesto=@uimpuesto 
	WHERE clave = @uclave
GO

IF EXISTS (SELECT name FROM sysobjects 
			WHERE name = 'eliminarMaterial' AND type='P')
	DROP PROCEDURE eliminarMaterial;

GO
CREATE PROCEDURE eliminarMaterial
	@uclave NUMERIC(5,0)
AS
	DELETE FROM Materiales WHERE clave = @uclave
GO

-- Procedures Para Tabla Proyectos
IF EXISTS (SELECT name FROM sysobjects
		   WHERE name = 'creaProyecto' AND type = 'P')
	DROP PROCEDURE creaProyecto;

GO
CREATE PROCEDURE creaProyecto
	@unumero NUMERIC(5,0),
	@udenominacion VARCHAR(50)
AS
	INSERT INTO Proyectos VALUES (@unumero, @udenominacion)

GO

IF EXISTS (SELECT name FROM sysobjects
		   WHERE name = 'modificarProyecto' AND type = 'P')
	DROP PROCEDURE modificarProyecto;

GO
CREATE PROCEDURE modificarProyecto
	@unumero NUMERIC(5,0),
	@udenominacion VARCHAR(50)
AS
	UPDATE Proyectos 
	SET Numero=@unumero, Denominacion=@udenominacion
	WHERE Numero = @unumero
GO

IF EXISTS (SELECT name FROM sysobjects 
			WHERE name = 'eliminarProyecto' AND type='P')
	DROP PROCEDURE eliminarProyecto;

GO
CREATE PROCEDURE eliminarProyecto
	@unumero NUMERIC(5,0)
AS
	DELETE FROM Proyectos WHERE Numero = @unumero
GO


-- Procedures Para Tabla Proveedores
IF EXISTS (SELECT name FROM sysobjects
		   WHERE name = 'creaProveedor' AND type = 'P')
	DROP PROCEDURE creaProveedor;

GO
CREATE PROCEDURE creaProveedor
	@urfc CHAR(13),
	@urazonsocial VARCHAR(50)
AS
	INSERT INTO Proveedores VALUES (@urfc, @urazonsocial)
GO

IF EXISTS (SELECT name FROM sysobjects
		   WHERE name = 'modificarProveedor' AND type = 'P')
	DROP PROCEDURE modificarProveedor;

GO
CREATE PROCEDURE modificarProveedor
	@urfc CHAR(13),
	@urazonsocial VARCHAR(50)
AS
	UPDATE Proveedores 
	SET RFC=@urfc, RazonSocial=@urazonsocial
	WHERE RFC = @urfc
GO

IF EXISTS (SELECT name FROM sysobjects 
			WHERE name = 'eliminarProveedores' AND type='P')
	DROP PROCEDURE eliminarProveedores;

GO
CREATE PROCEDURE eliminarProveedores
	@urfc CHAR(13)
AS
	DELETE FROM Proveedores WHERE RFC = @urfc
GO

-- Procedures Para Tabla Entregan
IF EXISTS (SELECT name FROM sysobjects
		   WHERE name = 'creaEntrega' AND type = 'P')
	DROP PROCEDURE creaEntrega;

GO
CREATE PROCEDURE creaEntrega
	@uclave NUMERIC(5),
	@urfc char(13),
	@unumero numeric(5),
	@ufecha datetime,
	@ucantidad numeric(8,2)
AS
	INSERT INTO Entregan VALUES (@uclave, @urfc, @unumero, @ufecha, @ucantidad)
GO

IF EXISTS (SELECT name FROM sysobjects
		   WHERE name = 'modificarEntrega' AND type = 'P')
	DROP PROCEDURE modificarEntrega;

GO
CREATE PROCEDURE modificarEntrega
	@uclave NUMERIC(5),
	@urfc char(13),
	@unumero numeric(5),
	@ufecha datetime,
	@ucantidad numeric(8,2)
AS
	UPDATE Entregan
	SET Clave=@uclave, RFC=@urfc, Numero=@unumero, Fecha=@ufecha, Cantidad=@ucantidad
	WHERE RFC = @urfc AND Clave=@uclave AND Numero=@unumero AND Fecha=@ufecha
GO

IF EXISTS (SELECT name FROM sysobjects 
			WHERE name = 'eliminarEntrega' AND type='P')
	DROP PROCEDURE eliminarEntrega;

GO
CREATE PROCEDURE eliminarEntrega
	@uclave NUMERIC(5),
	@urfc char(13),
	@unumero numeric(5),
	@ufecha datetime
AS
	DELETE FROM Entregan
	WHERE RFC = @urfc AND Clave=@uclave AND Numero=@unumero AND Fecha=@ufecha
GO


EXECUTE creaProveedor 'ABCD111122','Bimbo'
EXECUTE creaProveedor 'ABXX111122','FEMSA'

EXECUTE modificarProveedor 'ABCD111122','Bimbo Inc.'

SELECT * FROM Proveedores
WHERE RFC LIKE '[A-D]%'


SELECT * FROM Proveedores
-- Consultar Materiales
IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'queryMaterial' AND type = 'P')
    DROP PROCEDURE queryMaterial
GO

CREATE PROCEDURE queryMaterial
    @udescripcion VARCHAR(50),
    @ucosto NUMERIC(8,2)

AS
    SELECT * FROM Materiales WHERE descripcion
    LIKE '%'+@udescripcion+'%' AND costo > @ucosto
GO


EXECUTE queryMaterial 'Lad',20 

