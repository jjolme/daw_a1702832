create database fruits_db;

use fruits_db;

CREATE TABLE Fruit (
     name varchar(30) not null,
     units integer not null,
     quantity integer not null,
     price float(16) not null,
     country varchar(30) not null
);

INSERT INTO Fruit VALUES ('Manzana', 13, 20, 23.5, 'Mexico');
INSERT INTO Fruit VALUES ('Mango', 10, 22, 53.5, 'Mexico');
INSERT INTO Fruit VALUES ('Mandarina', 1, 2, 5.0, 'China');
INSERT INTO Fruit VALUES ('Melon', 13, 22, 10.0, 'China');
INSERT INTO Fruit VALUES ('Platano', 8, 12, 16.0, 'Jamaica');


INSERT INTO Images VALUES ('Manzana','apple.jpeg');
INSERT INTO Images VALUES ('Mango','mango.jpeg');
INSERT INTO Images VALUES ('Mandarina','mandarina.jpeg');
INSERT INTO Images VALUES ('Melon','sweetmelon.jpeg');
INSERT INTO Images VALUES ('Platano','banana.jpeg');

delimiter //

DROP PROCEDURE IF EXISTS InsertarFruta//
CREATE PROCEDURE InsertarFruta (IN uname varchar(30),
								IN uunits int(11),
								IN uquantity int(11),
								IN uprice float,
								IN ucountry varchar(30))
BEGIN
	INSERT INTO Fruit VALUES (uname, uunits, uquantity, uprice, ucountry);
END//


DROP PROCEDURE IF EXISTS GetImages//
CREATE PROCEDURE GetImages (IN uname varchar(30))
BEGIN
	SELECT * FROM Images WHERE name LIKE CONCAT('%',uname,'%');
END//


DROP PROCEDURE IF EXISTS GetAllFruits//
CREATE PROCEDURE GetAllFruits ()
BEGIN
	SELECT name, units, quantity, price, country FROM Fruit;
END//

delimiter ;

/* To Visualize Created Procedures

SELECT ROUTINE_NAME 
FROM INFORMATION_SCHEMA.ROUTINES 
WHERE 
       ROUTINE_TYPE="PROCEDURE" 
   AND ROUTINE_SCHEMA="fruits_db";
*/







