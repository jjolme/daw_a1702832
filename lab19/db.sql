create database fruits_db;

use fruits_db;

CREATE TABLE Fruit (
     name varchar(30) not null,
     units integer not null,
     quantity integer not null,
     price float(16) not null,
     country varchar(30) not null
);

INSERT INTO Fruit VALUES ('Manzana', 13, 20, 23.5, 'Mexico');
INSERT INTO Fruit VALUES ('Mango', 10, 22, 53.5, 'Mexico');
INSERT INTO Fruit VALUES ('Mandarina', 1, 2, 5.0, 'China');
INSERT INTO Fruit VALUES ('Melon', 13, 22, 10.0, 'China');
INSERT INTO Fruit VALUES ('Platano', 8, 12, 16.0, 'Jamaica')