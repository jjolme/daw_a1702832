
	<article>
		<h2>Fruits Mini App</h2>
		<div>
			<input type="userinput" name="userinput" id="userinput" onkeyup="sendRequest()">
			<br><br>
			<br><br>
			<br><br>
			<br><br>
			<br>
			<div id="ajaxResponse"></div>
		</div>
	</article>


	<!-- Start Section Questions -->
		<div class="separator" id="questions-separator">
			<h2>Questions</h2>
		</div>

		<article id="preg-1">
			<h2>¿Qué importancia tiene AJAX en el desarrollo de RIA's (Rich Internet Applications?</h2>
			<div>
				<p>
					AJAX habilita muchas posibilidades en el desarrollo de aplicaciones web. Transformó las páginas de lo que eran antes, simples documentos estáticos con formularios, a lo que son ahora, una aplicación rica de maneras de interactuar con el usuario.
				</p>
				<ul>
					<li>Hace posible mejorar la retroalimentación de acciones al usuario.</li>
					<li>Informarle de errores antes de ser validados en el servidor.</li>
					<li>Habilitar comportamientos dinámicos locales en el sitio.</li>
					<li>Efectuar cálculos y apps.</li>
					<li>Desarrollar Aplicaciones Web de Solo una Página en donde las páginas del sitio se cargue mediante llamadas remotas. </li>
					<li>Etc.</li>
				</ul>
			</div>
		</article>

		<article id="preg-2">
			<h2>¿Qué implicaciones de seguridad tiene AJAX? ¿Dónde se deben hacer las validaciones de seguridad, del lado del cliente o del lado del servidor?</h2>
			<div>
				<p>
					Las validaciones siempre como ya antes visto, deben hacerse del lado del servidor. Es posible que un atacante haya alterado el código JavaScript o que este atacando el sitio con peticiones sin incluso usar un navegador.
				</p>
				<br>
				<p>Sin embargo, las validaciones en el lado del cliente dan una mejor experiencia de uso al usuario. Y reducen el tiempo de espera de acción. Sin embargo, estas deben ser segundas validaciones. Las del servidor tienen mayor prioridad.</p>
			</div>
		</article>

		<article id="preg-3">
			<h2>¿Qué es JSON?</h2>
			<div>
				<p>
					JavaScript Object Notation es una especificación de Objetos de JavaScript con la cual se puede decodificar y codificar objetos de javascript a texto plano.
					<br>
					Esto es útil para intercambiar datos entre el cliente y servidor, ya que son legibles. facilmente de manipular en JS y en PHP, y muy poco costos en espacio.
				</p>
			</div>
		</article>

		<article id="preg-4">
			<h2>¿Qué es CSRF y cómo puede prevenirse?</h2>
			<div>
				<p>
					Cross Site Request Forgery es un tipo de ataque web en donde se hace engañar un usuario autenticado en un sitio con su navegador, para que haga peticiones perjudiciales al sitio web y que sean validadas por el sitio. [4]
					<br>
					Esto funciona porque los sitios web sólo verifican que el navegador del usuario este autenticado, no que realmente haya sido una acción válida del sitio ejercido por el usuario o que sea en el mismo sitio para empezar.
					<br>
					<ul>
						<li>Primer nivel de protección: usar peticiones POST en operaciones delicadas.</li>
						<li>Segundo nivel: Generar un 'token' aleatorio que será almacenado en el navegador del usuario, y que cada petición deba ser verificada con dicho 'token'.
					<br>
					Funciona porque los atacantes de CSRF no pueden ellos remotamente conseguir información de las cookies para incluirlas en la petición.</li>
					</ul>
				</p>
				
			</div>
		</article>
		<!-- End Section Questions -->
		<br>

		<footer>
			<h2>Bibliografía:</h2>
			<ul>
				<li>[1] <a href="https://canvas.seattlecentral.edu/courses/937693/pages/10-advanced-php-sessions">https://canvas.seattlecentral.edu/courses/937693/pages/10-advanced-php-sessions</a></li>
				<li>[2] <a href="https://stackoverflow.com/questions/24858858/is-it-safe-to-store-a-file-with-its-original-name-on-server-in-php">https://stackoverflow.com/questions/24858858/is-it-safe-to-store-a-file-with-its-original-name-on-server-in-php</a></li>
				<li>[3] <a href="https://www.opswat.com/blog/file-upload-protection-best-practices">https://www.opswat.com/blog/file-upload-protection-best-practices</a></li>
				<li>[4] <a href="https://medium.com/@GemmaBlack/csrf-the-a-z-2b85d26438b">https://medium.com/@GemmaBlack/csrf-the-a-z-2b85d26438b</a></li>
				<li>[5] <a href="https://stackoverflow.com/questions/4303311/what-is-the-difference-between-session-unset-and-session-destroy-in-php">https://stackoverflow.com/questions/4303311/what-is-the-difference-between-session-unset-and-session-destroy-in-php</a></li>
				
				
			</ul>
		</footer>
		<script type="text/javascript" src="ajax.js"></script>
	</div>
</body>
</html>