function getRequestObject() {

	if (window.XMLHttpRequest) {
		return (new XMLHttpRequest());
	} else if (window.ActiveXObject) {
		return (new ActiveXObject("Microsoft.XMLHTTP"));
	}

	return null;
}


function sendRequest() {
	request = getRequestObject();

	console.log("Sending Request!");

	if (request != null) {
		var userInput = document.getElementById('userinput').value;
		var url = 'ssajax.php?pattern='+userInput

		console.log("Pattern: " + userInput)
		request.open('GET', url, true);
		request.onreadystatechange =
			function() {
				if (request.readyState == 4) {

					var ajaxRes = document.getElementById('ajaxResponse');
					var res = request.responseText;
					ajaxRes.innerHTML = res;
					ajaxRes.style.visibility='visible';

					console.log("AJAX RESPONSE: " + res);
				}
			};

		request.send(null);

	}
}


function showSelect(e) {
	alert("Haz Seleccionado "+e.value+"!");
}




