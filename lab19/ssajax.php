<?php 

require_once('util.php');

$words = array();

$fruits = getAllFruits();

if (mysqli_num_rows($fruits) > 0) {
	while ($row = mysqli_fetch_assoc($fruits)) {
		array_push($words, $row['name']);
	}
}

$pattern = strtolower($_GET['pattern']);



$response = "";
$size = 0;

for ($i=0; $i < count($words); $i++) {
	$pos = stripos(strtolower($words[$i]), $pattern);
	if (!($pos === false)) {
		$size++;
		$word = $words[$i];
		$response .= "<option value=\"$word\">$word</option>";
	}
}

if ($size > 0) {
	echo "<select id=\"list\" size=$size onclick=\"showSelect(this)\">$response</select>";
}

 ?>