<?php 
require_once('util.php');

$fruitName = "Man";
$precio = 17;

function outputList($fruits) {
	$html = "";
	if (mysqli_num_rows($fruits) > 0) {
		while ($row = mysqli_fetch_assoc($fruits)) {
			$html .= "<li>";
			 $html .= "Nombre: ".$row['name']." - Precio: ".$row['price']." - Cantidad: ".$row['units'];
			$html .= "</li>";
		}
	}
	return $html;
}

 ?>


	<article>
		<h2>Lab de Frutas</h2>
		<div>
			<h4>Todas las frutas en la base de datos:</h4>
			<ul>
				<?= outputList(getAllFruits()) ?>
			</ul>

			<h4>Todas las frutas en la base de datos que empiezan con "<?= $fruitName ?>":</h4>
			<ul>
				<?= outputList(getFruitsByName($fruitName)) ?>
			</ul>

			<h4>Todas las frutas en la base de datos que cuestan menos de <?= $precio ?>$:</h4>
			<ul>
				<?= outputList(getFruitsCheaperThan($precio)) ?>
			</ul>
		</div>
	</article>


	<!-- Start Section Questions -->
		<div class="separator" id="questions-separator">
			<h2>Questions</h2>
		</div>

		<article id="preg-1">
			<h2>¿Qué es ODBC y para qué es útil?</h2>
			<div>
				<p>
					Es un estandard que permite accesar y ejecutar instrucciones en un sistema de base de datos independiente de que sistema sea. Esto se logra implementando una capa de interfaz entre la aplicación y los sistemas de bases de datos.
					Un derivado por ejemplo es JDBC, que sirve para conectar aplicaciones de Java con una base de datos. [3] 
				</p>
			</div>
		</article>

		<article id="preg-2">
			<h2>¿Qué es SQL Injection?</h2>
			<div>
				<p>
					SQL Injection es un tipo de ataque hacia un servidor en donde el atacante ejecuta consultas indeseadas y perjudiciales en la base de datos. Esto lo hace mediante poniendo en los campos de entrada caracteres especiales interpretados por la base de datos como sentencias SQL. De esta manera puede siempre validar que la consulta sea cierta y con permisos. O peor, ejecutar más instrucciones peligrosas como destruir tablas.[1]
				</p>
			</div>
		</article>

		<article id="preg-3">
			<h2>¿Qué técnicas puedes utilizar para evitar ataques de SQL Injection?</h2>
			<div>
				<p>
					Para protegerte en contra de SQL Injection generalmente basta con susitutuir los carácteres especiales en un formato no malicioso. La función mysqli_escape_string() hace esta sustitución. También, se pueden usar consultas preparadas con mysqli_prepare_statement() que, al insertar datos a la instrucción, se mandan por otro protocolo a la base de datos que no puede ser atacado de esta manera.[1]  y [2]
				</p>
			</div>
		</article>

		<!-- End Section Questions -->
		<br>

		<footer>
			<h2>Bibliografía:</h2>
			<ul>
				<li>[1] <a href="https://www.acunetix.com/websitesecurity/sql-injection/">https://www.acunetix.com/websitesecurity/sql-injection/</a></li>
				<li>[2] <a href="https://www.w3schools.com/php/php_mysql_prepared_statements.asp">https://www.w3schools.com/php/php_mysql_prepared_statements.asp</a></li>
				<li>[3] <a href="https://es.wikipedia.org/wiki/Open_Database_Connectivity">https://es.wikipedia.org/wiki/Open_Database_Connectivity</a></li>
				
				
				
			</ul>
		</footer>

	</div>
</body>
</html>